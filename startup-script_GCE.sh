sudo curl -fsSL https://get.docker.com -o get-docker.sh
sudo chmod +x get-docker.sh
sudo sh get-docker.sh
sudo curl -SL https://github.com/docker/compose/releases/download/v2.28.1/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
sudo mkdir /home/rabbit-service
sudo printf "services: \n" > /home/rabbit-service/docker-composer.yml
sudo printf "  rabbitmq: \n" >> /home/rabbit-service/docker-composer.yml
sudo printf "    image: rabbitmq:3-management \n" >> /home/rabbit-service/docker-composer.yml
sudo printf "    container_name: rabbitmq \n" >> /home/rabbit-service/docker-composer.yml
sudo printf "    volumes: \n" >> /home/rabbit-service/docker-composer.yml
sudo printf "      - /opt/vetapp2date_rabbit_data:/var/lib/rabbitmq \n" >> /home/rabbit-service/docker-composer.yml
sudo printf "    environment: \n" >> /home/rabbit-service/docker-composer.yml
sudo printf "      RABBITMQ_DEFAULT_USER: guest \n" >> /home/rabbit-service/docker-composer.yml
sudo printf "      RABBITMQ_DEFAULT_PASS: guest \n" >> /home/rabbit-service/docker-composer.yml
sudo printf "    ports: \n" >> /home/rabbit-service/docker-composer.yml
sudo printf "      - '5672:5672' \n" >> /home/rabbit-service/docker-composer.yml
sudo printf "      - '15672:15672' \n" >> /home/rabbit-service/docker-composer.yml
sudo /usr/bin/docker-compose -f /home/rabbit-service/docker-composer.yml up -d
