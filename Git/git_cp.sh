#!/usr/bin/env bash

src_git=$1
dst_git=$2
branch=$3
#var=`echo $?`
dir_git=`echo $src_git | awk -F "/" '{print $NF}' |  awk -F ".git" '{print $1}' `


echo $dir_git

# Check if arg $1 is null
if [ -n "$1" ]
then
echo "You are entered git source: $src_git"
else
echo
echo "Please, entered command in format COMMAND git-source git-destination branch-name"
echo "Example: test.sh git@community.opengroup.org:osdu/platform/system/lib/core/os-core-common.git git@git.epam.com:go3-nrg/platform/System/lib/core/os-core-common.git release/0.2"
exit 256
fi

# Main script
cd  $HOME

git clone $src_git
cd $dir_git

git remote add upstream $dst_git
git checkout $branch
git push -u upstream --force $branch
git remote remove upstream
cd $HOME && rm -rf $dir_git

if [ `echo $?` -eq 0 ]

then
echo "Done"

else
echo "Something going wrong"

fi


