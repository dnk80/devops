import json
import requests

prj_src = input('Enter source Project ID: ')
prj_dst = input('Enter destination Project ID: ')
token = ''

url1 = 'https://git.epam.com/api/v4/projects/'
url2 = '/variables'
url_src = url1 + prj_src + url2
url_dst = url1 + prj_dst + url2

headers = {'PRIVATE-TOKEN': token}
read = requests.get(url_src, headers=headers)

data = json.loads(read.text)

for i in range(0,len(data)):

    data_url = {'key': data[i]['key'], 'value': data[i]['value']}
    write = requests.post(url_dst, headers=headers, data=data_url)

print(len(data),  "variables copied from")
print("Source project:", url_src)
print("Destination project:", url_dst)
