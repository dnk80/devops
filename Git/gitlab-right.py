import json
import requests

prj_dst = 88435
#prj_dst = input('Enter Project ID: ')

#token = input('Enter Personal-Token: ')
# 88435 my test project


url1 = 'https://git.epam.com/api/v4/projects/'
url_dst_prj = url1 + str(prj_dst)

headers = {'PRIVATE-TOKEN': token}

url_dst_branch = url1 + str(prj_dst) + '/repository/branches'

url_dst_prj_branch_protect = url1 + str(prj_dst) + '/protected_branches'

branch_list = ['integration-master','integration-release/0.2','integration-release/0.3','release/0.2','release/0.3']

# Create branch and set default branch for repository

for i in range(0,len(branch_list)):
    data_url_branch = {'branch': branch_list[i], 'ref': 'master'}
    ## Желательно переделать - ссылаться на дефаулт бранч с json что бы не было затыка с нарезкой бренчей если master-а не существует
    write_branch = requests.post(url_dst_branch, headers=headers, data=data_url_branch)
    print('Branch ',branch_list[i], ' created' )

data_url_permis = {
    'default_branch':'integration-master',
    'lfs_enabled':'false',
    'only_allow_merge_if_pipeline_succeeds':'true',
    'only_allow_merge_if_all_discussions_are_resolved':'true',
    'remove_source_branch_after_merge':'false',
    'ci_config_path':'opengroup-default-pull-sync.gitlab-ci.yml@go3-nrg/cicd-config',
    'wiki_enabled':'false', 'wiki_access_level':'disabled',
    'snippets_enabled':'false', 'snippets_access_level':'disabled',
    'issues_enabled':'false', 'issues_access_level':'disabled'}

write = requests.put(url_dst_prj, headers=headers, data=data_url_permis)
print('Permission to Project applied')
print('Default Branch Set')
print("To check Project Permission go to:", json.loads(requests.get(url1 + str(prj_dst), headers=headers).text)['web_url'])

#read = requests.get(url_dst_prj, headers=headers) # Чтение прав проекта
##print(read.text)
############# End create branch and set default branch for repository#################


################# Give correct right on branches #####################################

branch_mask = ['master','integration-master','integration-release/*','release/*']
for a in range(0,len(branch_mask)):
    requests.delete(url_dst_prj_branch_protect + '/' + branch_mask[a], headers=headers)

#requests.delete(url_dst_prj_branch_protect + '/master', headers=headers)

#dt = {'name':'master', 'push_access_level':'40', 'merge_access_level':'0', 'unprotect_access_level':'40'}
##requests.delete(url_dst_prj_branch_protect+'/feature1-*', headers=headers)
requests.post(url_dst_prj_branch_protect, headers=headers, data={'name':'master', 'push_access_level':'40', 'merge_access_level':'0', 'unprotect_access_level':'40'})  #PUT не работает для нарезки изменения правд бранча, только удаление и создание заново
requests.post(url_dst_prj_branch_protect, headers=headers, data={'name':'release/*', 'push_access_level':'40', 'merge_access_level':'0', 'unprotect_access_level':'40'})
requests.post(url_dst_prj_branch_protect, headers=headers, data={'name':'integration-master', 'push_access_level':'40', 'merge_access_level':'40', 'unprotect_access_level':'40'})
requests.post(url_dst_prj_branch_protect, headers=headers, data={'name':'integration-release/*', 'push_access_level':'40', 'merge_access_level':'40', 'unprotect_access_level':'40'})
print('Protected Branches Set')
################# End Give correct right on branches ##################################

#read = requests.get(url_dst_prj_branch_protect, headers=headers) # Чтение прав бранча
#print(read.text) # Чтение прав бранча


############################ Jira integration #########################################
data_url_jira = {
'url':'https://jiraeu-api.epam.com',
'api_url':'https://jiraeu-api.epam.com',
'username':'Auto_GO3-NRG_Ticket',
'password': 'skbTAJgahPHry7CM8uezCMJ5T',
'active':'true',
'commit_events':'true',
'merge_requests_events':'true',
'comment_on_event_enabled':'true'
}

requests.put(url_dst_prj + '/services/jira', headers=headers, data=data_url_jira)
#PUT /projects/:id/services/jira

##read = requests.get(url_dst_prj + '/services/jira', headers=headers)
##print(read.text)
print('Notification to Jira Enabled')
############################ End Jira integration ######################################


data_url_msteams = {
'webhook':'https://outlook.office.com/webhook/acd099c5-0d41-4be8-95d8-07543c477e54@b41b72d0-4e9f-4c26-8a69-f949f367c91d/IncomingWebhook/2c112bc5dc4e4b298bd2502e82aa204c/7eca8ded-369a-41b7-a877-0899a212a01b'
}

requests.put(url_dst_prj + '/services/microsoft-teams', headers=headers, data=data_url_msteams)
#PUT /projects/:id/services/jira

##read = requests.get(url_dst_prj + '/services/microsoft-teams', headers=headers)
#print(read.text)
print('Notification to Microsoft Teams Enabled')
############################ End Jira integration ######################################

write = requests.delete(url_dst_prj, headers=headers)