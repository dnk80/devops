sudo apt-get update
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo apt-get install gitlab-runner -yq
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "GR1348941czXXXXTCi6rR7Kz92qyC" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner" \
  --maintenance-note "Free-form maintainer notes about this runner" \
  --tag-list "smartrr-gcp-qa" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
sudo printf '#!/bin/sh \n' > /etc/cron.daily/docker_clear_container
sudo printf "set -e \n" >> /etc/cron.daily/docker_clear_container
sudo printf 'docker rmi $(docker images 'gcr.io/deft-station-288204/smartrr/autotest' -a -q) \n' >> /etc/cron.daily/docker_clear_container
sudo printf 'docker rmi $(docker images 'google/cloud-sdk' -a -q) \n' >> /etc/cron.daily/docker_clear_container
sudo chmod +x /etc/cron.daily/docker_clear_container
